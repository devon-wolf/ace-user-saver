import { render } from '@testing-library/react';
import UserRow from './user-row';

describe('UserRow', () => {
    it('should render successfully', () => {
        const { baseElement } = render(<UserRow user={
            {
                login: 'devon-wolf',
                id: 33987744,
                url: 'https://github.com/devon-wolf',
                name: 'Devon Wolfkiel',
                public_repos: 1,
                public_gists: 1,
                followers: 1,
                following: 1,
                created_at: '11/25/2017'
            }
        }/>);
        expect(baseElement).toBeTruthy();
    });
});
