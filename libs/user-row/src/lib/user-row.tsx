import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Link from '@material-ui/core/Link';
import { User } from '@nx-user-saver/types';
import './user-row.module.css';

export interface UserRowProps {
  user: User
}

export function UserRow({ user }: UserRowProps) {
    const { login, id, url, name, public_repos, public_gists, followers, following, created_at } = user;
    
    return (
        <TableRow>
            <TableCell>
                <Link href={url}>{login}</Link>
            </TableCell>

            <TableCell>
                {name}
            </TableCell>

            <TableCell>
                {id}
            </TableCell>

            <TableCell>
                {public_repos}
            </TableCell>

            <TableCell>
                {public_gists}
            </TableCell>

            <TableCell>
                {followers}
            </TableCell>

            <TableCell>
                {following}
            </TableCell>

            <TableCell>
                {created_at}
            </TableCell>
        </TableRow>
    );
}

export default UserRow;
