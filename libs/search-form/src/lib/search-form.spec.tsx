import { render } from '@testing-library/react';

import SearchForm from './search-form';

describe('SearchForm', () => {
    it('should render successfully', () => {
        const { baseElement } = render(<SearchForm handleUserFetch={() => console.log('user fetch placeholder')} />);
        expect(baseElement).toBeTruthy();
    });
});
