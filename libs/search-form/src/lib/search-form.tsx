import React, { useState, FormEvent } from 'react';
import TextField from '@material-ui/core/TextField';
import { User } from '@nx-user-saver/types';
import style from './search-form.module.css';

export interface SearchFormProps {
  handleUserFetch: (result: User) => void;
}

export function SearchForm({ handleUserFetch }: SearchFormProps) {
    const [searchInput, setSearchInput] = useState('');
    const [helperText, setHelperText] = useState('Enter a GitHub username');
    const [error, setError] = useState(false);

    const handleFormSubmit = (e: FormEvent) => {
        e.preventDefault();   
        setSearchInput('');
    };

    return (
        <form
            className={style.searchForm}
            onSubmit={handleFormSubmit}
        >
            <TextField
                variant="filled"
                inputProps={{ 'aria-label': 'user search'}}
                label="User Search"
                helperText={helperText}
                fullWidth={true}
                value={searchInput}
                onChange={e => setSearchInput(e.target.value)}
                color={error ? 'secondary' : 'primary'}
            /> 
        </form>
    );
}

export default SearchForm;
