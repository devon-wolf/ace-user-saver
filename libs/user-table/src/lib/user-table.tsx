import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import { User } from '@nx-user-saver/types';
import { UserRow } from '@nx-user-saver/user-row';
import './user-table.module.css';

export interface UserTableProps {
  users: User[];
}

export function UserTable({ users }: UserTableProps) {
    return (
        <Table aria-label="saved users">
            <TableHead>
                <TableRow>
                    <TableCell>Username</TableCell>
                    <TableCell>Name</TableCell>
                    <TableCell>GitHub ID</TableCell>
                    <TableCell>Public Repos</TableCell>
                    <TableCell>Public Gists</TableCell>
                    <TableCell>Followers</TableCell>
                    <TableCell>Following</TableCell>
                    <TableCell>Created At</TableCell>
                </TableRow>
            </TableHead>

            <TableBody>
                {users.map(user =>
                    <UserRow
                        key={user.id}
                        user={user}
                    />
                )}
            </TableBody>
        </Table>
    );
}

export default UserTable;
