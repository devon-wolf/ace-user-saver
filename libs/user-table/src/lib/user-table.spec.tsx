import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import UserTable from './user-table';

const users = [
    {
        login: 'devon-wolf',
        id: 1,
        url: 'https://github.com/devon-wolf',
        name: 'Devon Wolfkiel',
        public_repos: 1,
        public_gists: 1,
        followers: 1,
        following: 1,
        created_at: '11/25/2017'
    },
    {
        login: 'devon-wolf',
        id: 2,
        url: 'https://github.com/devon-wolf',
        name: 'Devon Wolfkiel',
        public_repos: 1,
        public_gists: 1,
        followers: 1,
        following: 1,
        created_at: '11/25/2017'
    }
];

describe('UserTable', () => {
    
    it('should render successfully', () => {
        const { baseElement } = render(<UserTable users={users}/>);
        expect(baseElement).toBeTruthy();
    });
    
    beforeEach(() => {
        render(<UserTable users={users}/>);
    });

    it('has the required columns', () => {
        screen.getByText('Username');
        screen.getByText('GitHub ID');
        screen.getByText('Name');
        screen.getByText('Public Repos');
        screen.getByText('Public Gists');
        screen.getByText('Followers');
        screen.getByText('Following');
        screen.getByText('Created At');
    });

    it('has a row for each user', () => {
        const nonHeadingRows = screen.getAllByRole('row').slice(1);
        expect(nonHeadingRows.length).toEqual(users.length);
    });

    it('matches the sample snapshot when using the sample data', () => {
        expect(screen.getByRole('table')).toMatchSnapshot();
    });
});
