import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import UserSaver from './pages/UserSaver';
import App from './app';
import sampleUsers from './data/users';

describe('App', () => {
    it('should render successfully', () => {
        const { baseElement } = render(<App />);
        expect(baseElement).toBeTruthy();
    });
});

describe('User saver', () => {
    beforeEach(() => {
        render(<UserSaver />);
    });

    it('renders a search bar', () => {
        screen.getByLabelText('user search');
    });

    it('renders a table of saved users', () => {
        screen.getByLabelText('saved users');
    });
});
