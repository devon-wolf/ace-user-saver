import { fetchGitHubUser } from '../services/github';

describe.skip('Fetch GitHub user', () => {
    
    it('returns a user object when given a valid username', async () => {
        const response = await fetchGitHubUser('devon-wolf');
        expect(response).toEqual({
            login: 'devon-wolf',
            id: 33987744,
            url: 'https://github.com/devon-wolf',
            name: 'Devon Wolfkiel',
            public_repos: expect.any(Number),
            public_gists: expect.any(Number),
            followers: expect.any(Number),
            following: expect.any(Number),
            created_at: '11/25/2017'
        });
    });

    it('returns an error message when given an invalid username', async () => {
        const response = await fetchGitHubUser('averyveryfakeusername');
        expect(response).toEqual({ message: 'That user could not be found' });
    });
    
});
