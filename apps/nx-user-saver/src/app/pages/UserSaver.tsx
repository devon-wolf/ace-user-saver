import { Container } from '@material-ui/core';
import React, { useState } from 'react';
import { SearchForm } from '@nx-user-saver/search-form';
import { UserTable } from '@nx-user-saver/user-table';
import seedUsers from '../data/users';
import { User } from '@nx-user-saver/types';

const UserSaver = () => {
    const [users, setUsers] = useState(seedUsers);
    // down the line, will populate 'users' via a useEffect that pulls from persistent list

    const handleUserFetch = (result: User) => {
        setUsers([...users, result]);
    };

    return (
        <Container maxWidth="md">
            <SearchForm handleUserFetch={handleUserFetch}/>
            <UserTable users={users}/>
        </Container>
    );
};

export default UserSaver;
