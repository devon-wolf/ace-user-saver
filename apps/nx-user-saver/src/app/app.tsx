import { Container, CssBaseline } from '@material-ui/core';
import UserSaver from './pages/UserSaver';

export function App() {
    return (
        <>
            <CssBaseline />
            <Container maxWidth="md">
                <UserSaver />
            </Container>
        </>
    );
}

export default App;
