import { GitHubUser, User } from '@nx-user-saver/types';

const formatDate = (datestring: string): string => {
    const splitDate = datestring.split('-');
    return (
        splitDate[1] + 
        '/' + 
        splitDate[2].slice(0, 2) + 
        '/' +
        splitDate[0]
    );
};

const shapeUser = (user: GitHubUser): User => {
    const { login, id, html_url, name, public_repos, public_gists, followers, following, created_at } = user;

    return {
        login,
        id,
        url: html_url,
        name,
        public_repos,
        public_gists,
        followers,
        following,
        created_at: formatDate(created_at)
    };
};

export default shapeUser;
