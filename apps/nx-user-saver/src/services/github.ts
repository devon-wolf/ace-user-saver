import { GitHubUser, Message, User } from '@nx-user-saver/types';
import shapeUser from './shapeUser';

// on hold
export const fetchGitHubUser = async (searchTerm: string): Promise<User | Message> => {
    try {
        const response = await fetch(`https://api.github.com/users/${searchTerm}`);
        
        if (response.status === 404) return { message: 'That user could not be found' };
        else if (!response.ok) return { message: `${response.status}: Something unexpected happened` };
        
        const rawUser = await response.json() as GitHubUser;
        return shapeUser(rawUser);
    }
    catch (error) {
        console.log(error);
        return { message: 'Something went wrong with the request' };
    }
};
